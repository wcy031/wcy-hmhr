import { setToken, getToken, removeToken } from '@/utils/auth'
import { loginAPI, getUserProfileAPI, getUserPhotoAPI } from '@/api'
// import store from '@/store'
import router from '@/router'
const getDefaultState = () => {
  return {
    token: getToken() || '', // 用户token
    // name: '', // 姓名
    // avatar: '' // 用户头像
    userInfo: {} // 用户姓名头像
  }
}

const state = getDefaultState()

const mutations = {
  RESET_STATE: (state) => {
    Object.assign(state, getDefaultState())
  },
  SET_TOKEN: (state, newToken) => {
    state.token = newToken
    setToken(newToken)
  },
  REMOVE_TOKEN: (state) => {
    state.token = ''
    removeToken()
  },
  // 设置
  SET_USER: (state, value) => {
    state.userInfo = value
  },
  // 删除
  REMOVE_USER: (state) => {
    state.userInfo = {}
  }
}

const actions = {
  // 登录接口
  async loginActions({ commit }, value) {
    const [err, res] = await loginAPI(value)
    // 只有成功才需要保存token 无论最后显示什么  都要把数组返回到 逻辑页面
    if (!err) commit('SET_TOKEN', res.data)
    // 相当于用解构err res 变量再重新返回一个数组给逻辑页面
    return [err, res]
  },
  // 用户信息接口
  async getUserInfoActions({ commit }) {
    const res = await getUserProfileAPI()
    const res2 = await getUserPhotoAPI(res.data.userId)
    // console.log(res2)
    // 将用户名铺到vuex上
    commit('SET_USER', { ...res.data, ...res2.data })
  },
  // 退出登录
  logoutActions({ commit }) {
    // 点击退出登录 清除本地 vuex的token 跳转到登录页
    removeToken() // 清除本地token
    commit('REMOVE_TOKEN') // 清除vuex的token
    router.replace(`/login?redirect=${router.currentRoute.fullPath}`) // 跳转到登录页
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}

