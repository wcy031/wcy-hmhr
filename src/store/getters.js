// 所有的state不直接给逻辑页面使用  统统在getters中取值然后返回到外面getter属性
// 这里的形参state是整个store根实例上的state  要想访问某个模块内局部state变量
// 格式：state.模块名.内部属性名
const getters = {
  sidebar: state => state.app.sidebar,
  device: state => state.app.device,
  token: state => state.user.token,
  avatar: state => state.user.userInfo.staffPhoto,
  name: state => state.user.userInfo.username,
  companyId: state => state.user.userInfo.companyId
}
export default getters
