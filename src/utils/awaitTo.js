export const to = (pA) => {
  // 传入原来Promise对象   负责成功/失败的接收
  const pB = pA.then(res => {
    console.log(res)
    return [null, res]
  }).catch(err => {
    console.error(err)
    return [err, null]
  })
  return pB // 返回到to函数调用的地方
}
