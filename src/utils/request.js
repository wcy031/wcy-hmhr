import axios from 'axios'
import { Message } from 'element-ui'
import store from '@/store'
// import { removeToken } from '@/utils/auth'
// import router from '@/router'
const service = axios.create({
  baseURL: process.env.VUE_APP_BASE_API, // 开发环境 url
  timeout: 5000
})

// request interceptor
service.interceptors.request.use(
  config => {
    // 统一携带token
    const token = store.getters.token
    if (token) {
      // 携带token
      config.headers.Authorization = `Bearer ${token}`
    }
    return config
  },
  error => {
    return Promise.reject(error)
  }
)

// response interceptor
service.interceptors.response.use(
  response => { // 成功2xx 3xx
    const { success, message } = response.data // 这个data是axios内部
    if (success) {
      return response.data // 直接返回后端数据, 省略一层data
    } else {
      Message.error(message) // http状态码2xx, 但是逻辑错误
      return Promise.reject(message) // 返回Promise错误的对象
    }
  },
  error => { // 失败  4xx 5xx
    console.dir(error)
    // token过期需要跳转到登录页面 清除本地token  vuex的token
    if (error?.response?.data?.code === 10002) {
      store.dispatch('user/logoutActions')
    }
    Message.error(error)
    return Promise.reject(error)
  }
)

export default service
