import router from './router'
import store from '@/store'
const whiteList = ['/login', '/404'] // 白名单
// 导入标题
import getPageTitle from '@/utils/get-page-title'
// 导入进度条插件
import NProgress from 'nprogress'
// 导入进度条样式
import 'nprogress/nprogress.css'
router.beforeEach(async(to, from, next) => {
  // 显示进度条效果
  NProgress.start()
  // 如果又token登录页放行 则去别的页面  没登录  则强制回登录页
  const token = store.getters.token
  if (token) {
    // 确保登录 再获取用户信息
    if (!store.getters.name) await store.dispatch('user/getUserInfoActions')
    // 如果当前页面在登录页  则跳转首页
    if (to.path === '/login') {
      next('/')
    } else {
      next()
    }
  } else { // 如果没有token  则返回登录页 未登录
    // 如果为白名单里的token 则放行 否则 强制退回登录页
    if (whiteList.includes(to.path)) {
      next()
    } else {
      next('/login')
    }
  }
})

router.afterEach(() => {
  // 想要更改标题 先要找到标题在哪里  查找如何动态修改标题  按部就班
  // 拿到标题 将标题赋给document.title
  // router.currentRoute 获取当前路由路径对象
  document.title = getPageTitle(router.currentRoute.meta.title)
  // 隐藏进度条效果
  NProgress.done()
})
