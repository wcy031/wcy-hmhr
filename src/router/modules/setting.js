import Layout from '@/layout'
export default {
  path: '/setting', // 公司设置
  component: Layout,
  children: [
    {
      path: '',
      name: 'Setting',
      component: () => import('@/views/setting'),
      meta: { title: '公司设置', icon: 'setting' }
    }
  ]
}
